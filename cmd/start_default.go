//go:build !windows && !darwin

package cmd

import (
	"context"
	"fmt"

	"gitlab.com/a4to/almcli/api"
)

func startApp(ctx context.Context, client *api.Client) error {
	return fmt.Errorf("could not connect to almcli server, run 'almcli serve' to start it")
}
