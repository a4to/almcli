package tray

import (
	"gitlab.com/a4to/almcli/app/tray/commontray"
	"gitlab.com/a4to/almcli/app/tray/wintray"
)

func InitPlatformTray(icon, updateIcon []byte) (commontray.OllamaTray, error) {
	return wintray.InitTray(icon, updateIcon)
}
