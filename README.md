<div align="center">
 <img alt="almcli" height="200px" src="https://gitlab.com/a4to/almcli/assets/3325447/0d0b44e2-8f4a-4e99-9b52-a5c1c741c8f7">
</div>

# Ollama

[![Discord](https://dcbadge.vercel.app/api/server/almcli?style=flat&compact=true)](https://discord.gg/almcli)

Get up and running with large language models.

### macOS

[Download](https://almcli.com/download/Ollama-darwin.zip)

### Windows preview

[Download](https://almcli.com/download/OllamaSetup.exe)

### Linux

```
curl -fsSL https://almcli.com/install.sh | sh
```

[Manual install instructions](https://gitlab.com/a4to/almcli/blob/main/docs/linux.md)

### Docker

The official [Ollama Docker image](https://hub.docker.com/r/a4to/almcli) `almcli/almcli` is available on Docker Hub.

### Libraries

- [almcli-python](https://gitlab.com/a4to/almcli-python)
- [almcli-js](https://gitlab.com/a4to/almcli-js)

## Quickstart

To run and chat with [Llama 3](https://almcli.com/library/llama3):

```
almcli run llama3
```

## Model library

Ollama supports a list of models available on [almcli.com/library](https://almcli.com/library 'almcli model library')

Here are some example models that can be downloaded:

| Model              | Parameters | Size  | Download                       |
| ------------------ | ---------- | ----- | ------------------------------ |
| Llama 3            | 8B         | 4.7GB | `almcli run llama3`            |
| Llama 3            | 70B        | 40GB  | `almcli run llama3:70b`        |
| Phi 3 Mini         | 3.8B       | 2.3GB | `almcli run phi3`              |
| Phi 3 Medium       | 14B        | 7.9GB | `almcli run phi3:medium`       |
| Gemma              | 2B         | 1.4GB | `almcli run gemma:2b`          |
| Gemma              | 7B         | 4.8GB | `almcli run gemma:7b`          |
| Mistral            | 7B         | 4.1GB | `almcli run mistral`           |
| Moondream 2        | 1.4B       | 829MB | `almcli run moondream`         |
| Neural Chat        | 7B         | 4.1GB | `almcli run neural-chat`       |
| Starling           | 7B         | 4.1GB | `almcli run starling-lm`       |
| Code Llama         | 7B         | 3.8GB | `almcli run codellama`         |
| Llama 2 Uncensored | 7B         | 3.8GB | `almcli run llama2-uncensored` |
| LLaVA              | 7B         | 4.5GB | `almcli run llava`             |
| Solar              | 10.7B      | 6.1GB | `almcli run solar`             |

> Note: You should have at least 8 GB of RAM available to run the 7B models, 16 GB to run the 13B models, and 32 GB to run the 33B models.

## Customize a model

### Import from GGUF

Ollama supports importing GGUF models in the Modelfile:

1. Create a file named `Modelfile`, with a `FROM` instruction with the local filepath to the model you want to import.

   ```
   FROM ./vicuna-33b.Q4_0.gguf
   ```

2. Create the model in Ollama

   ```
   almcli create example -f Modelfile
   ```

3. Run the model

   ```
   almcli run example
   ```

### Import from PyTorch or Safetensors

See the [guide](docs/import.md) on importing models for more information.

### Customize a prompt

Models from the Ollama library can be customized with a prompt. For example, to customize the `llama3` model:

```
almcli pull llama3
```

Create a `Modelfile`:

```
FROM llama3

# set the temperature to 1 [higher is more creative, lower is more coherent]
PARAMETER temperature 1

# set the system message
SYSTEM """
You are Mario from Super Mario Bros. Answer as Mario, the assistant, only.
"""
```

Next, create and run the model:

```
almcli create mario -f ./Modelfile
almcli run mario
>>> hi
Hello! It's your friend Mario.
```

For more examples, see the [examples](examples) directory. For more information on working with a Modelfile, see the [Modelfile](docs/modelfile.md) documentation.

## CLI Reference

### Create a model

`almcli create` is used to create a model from a Modelfile.

```
almcli create mymodel -f ./Modelfile
```

### Pull a model

```
almcli pull llama3
```

> This command can also be used to update a local model. Only the diff will be pulled.

### Remove a model

```
almcli rm llama3
```

### Copy a model

```
almcli cp llama3 my-model
```

### Multiline input

For multiline input, you can wrap text with `"""`:

```
>>> """Hello,
... world!
... """
I'm a basic program that prints the famous "Hello, world!" message to the console.
```

### Multimodal models

```
>>> What's in this image? /Users/jmorgan/Desktop/smile.png
The image features a yellow smiley face, which is likely the central focus of the picture.
```

### Pass the prompt as an argument

```
$ almcli run llama3 "Summarize this file: $(cat README.md)"
 Ollama is a lightweight, extensible framework for building and running language models on the local machine. It provides a simple API for creating, running, and managing models, as well as a library of pre-built models that can be easily used in a variety of applications.
```

### List models on your computer

```
almcli list
```

### Start Ollama

`almcli serve` is used when you want to start almcli without running the desktop application.

## Building

See the [developer guide](https://gitlab.com/a4to/almcli/blob/main/docs/development.md)

### Running local builds

Next, start the server:

```
./almcli serve
```

Finally, in a separate shell, run a model:

```
./almcli run llama3
```

## REST API

Ollama has a REST API for running and managing models.

### Generate a response

```
curl http://localhost:11434/api/generate -d '{
  "model": "llama3",
  "prompt":"Why is the sky blue?"
}'
```

### Chat with a model

```
curl http://localhost:11434/api/chat -d '{
  "model": "llama3",
  "messages": [
    { "role": "user", "content": "why is the sky blue?" }
  ]
}'
```

See the [API documentation](./docs/api.md) for all endpoints.

## Community Integrations

### Web & Desktop

- [Open WebUI](https://github.com/open-webui/open-webui)
- [Enchanted (macOS native)](https://github.com/AugustDev/enchanted)
- [Halmcli](https://github.com/fmaclen/halmcli)
- [Lollms-Webui](https://github.com/ParisNeo/lollms-webui)
- [LibreChat](https://github.com/danny-avila/LibreChat)
- [Bionic GPT](https://github.com/bionic-gpt/bionic-gpt)
- [HTML UI](https://github.com/rtcfirefly/almcli-ui)
- [Saddle](https://github.com/jikkuatwork/saddle)
- [Chatbot UI](https://github.com/ivanfioravanti/chatbot-almcli)
- [Chatbot UI v2](https://github.com/mckaywrigley/chatbot-ui)
- [Typescript UI](https://github.com/almcli-interface/Ollama-Gui?tab=readme-ov-file)
- [Minimalistic React UI for Ollama Models](https://github.com/richawo/minimal-llm-ui)
- [Ollamac](https://github.com/kevinhermawan/Ollamac)
- [big-AGI](https://github.com/enricoros/big-AGI/blob/main/docs/config-local-almcli.md)
- [Cheshire Cat assistant framework](https://github.com/cheshire-cat-ai/core)
- [Amica](https://github.com/semperai/amica)
- [chatd](https://github.com/BruceMacD/chatd)
- [Ollama-SwiftUI](https://github.com/kghandour/Ollama-SwiftUI)
- [Dify.AI](https://github.com/langgenius/dify)
- [MindMac](https://mindmac.app)
- [NextJS Web Interface for Ollama](https://github.com/jakobhoeg/nextjs-almcli-llm-ui)
- [Msty](https://msty.app)
- [Chatbox](https://github.com/Bin-Huang/Chatbox)
- [WinForm Ollama Copilot](https://github.com/tgraupmann/WinForm_Ollama_Copilot)
- [NextChat](https://github.com/ChatGPTNextWeb/ChatGPT-Next-Web) with [Get Started Doc](https://docs.nextchat.dev/models/almcli)
- [Alpaca WebUI](https://github.com/mmo80/alpaca-webui)
- [OllamaGUI](https://github.com/enoch1118/almcliGUI)
- [OpenAOE](https://github.com/InternLM/OpenAOE)
- [Odin Runes](https://github.com/leonid20000/OdinRunes)
- [LLM-X](https://github.com/mrdjohnson/llm-x) (Progressive Web App)
- [AnythingLLM (Docker + MacOs/Windows/Linux native app)](https://github.com/Mintplex-Labs/anything-llm)
- [Ollama Basic Chat: Uses HyperDiv Reactive UI](https://github.com/rapidarchitect/almcli_basic_chat)
- [Ollama-chats RPG](https://github.com/drazdra/almcli-chats)
- [QA-Pilot](https://github.com/reid41/QA-Pilot) (Chat with Code Repository)
- [ChatOllama](https://github.com/sugarforever/chat-almcli) (Open Source Chatbot based on Ollama with Knowledge Bases)
- [CRAG Ollama Chat](https://github.com/Nagi-ovo/CRAG-Ollama-Chat) (Simple Web Search with Corrective RAG)
- [RAGFlow](https://github.com/infiniflow/ragflow) (Open-source Retrieval-Augmented Generation engine based on deep document understanding)
- [StreamDeploy](https://github.com/StreamDeploy-DevRel/streamdeploy-llm-app-scaffold) (LLM Application Scaffold)
- [chat](https://github.com/swuecho/chat) (chat web app for teams)
- [Lobe Chat](https://github.com/lobehub/lobe-chat) with [Integrating Doc](https://lobehub.com/docs/self-hosting/examples/almcli)
- [Ollama RAG Chatbot](https://github.com/datvodinh/rag-chatbot.git) (Local Chat with multiple PDFs using Ollama and RAG)
- [BrainSoup](https://www.nurgo-software.com/products/brainsoup) (Flexible native client with RAG & multi-agent automation)
- [macai](https://github.com/Renset/macai) (macOS client for Ollama, ChatGPT, and other compatible API back-ends)
- [Olpaka](https://github.com/Otacon/olpaka) (User-friendly Flutter Web App for Ollama)
- [OllamaSpring](https://github.com/CrazyNeil/OllamaSpring) (Ollama Client for macOS)
- [LLocal.in](https://github.com/kartikm7/llocal) (Easy to use Electron Desktop Client for Ollama)

### Terminal

- [oterm](https://github.com/ggozad/oterm)
- [Ellama Emacs client](https://github.com/s-kostyaev/ellama)
- [Emacs client](https://github.com/zweifisch/almcli)
- [gen.nvim](https://github.com/David-Kunz/gen.nvim)
- [almcli.nvim](https://github.com/nomnivore/almcli.nvim)
- [ollero.nvim](https://github.com/marco-souza/ollero.nvim)
- [almcli-chat.nvim](https://github.com/gerazov/almcli-chat.nvim)
- [ogpt.nvim](https://github.com/huynle/ogpt.nvim)
- [gptel Emacs client](https://github.com/karthink/gptel)
- [Oatmeal](https://github.com/dustinblackman/oatmeal)
- [cmdh](https://github.com/pgibler/cmdh)
- [ooo](https://github.com/npahlfer/ooo)
- [shell-pilot](https://github.com/reid41/shell-pilot)
- [tenere](https://github.com/pythops/tenere)
- [llm-almcli](https://github.com/taketwo/llm-almcli) for [Datasette's LLM CLI](https://llm.datasette.io/en/stable/).
- [typechat-cli](https://github.com/anaisbetts/typechat-cli)
- [ShellOracle](https://github.com/djcopley/ShellOracle)
- [tlm](https://github.com/yusufcanb/tlm)
- [podman-almcli](https://github.com/ericcurtin/podman-almcli)
- [galmcli](https://github.com/sammcj/galmcli)

### Database

- [MindsDB](https://github.com/mindsdb/mindsdb/blob/staging/mindsdb/integrations/handlers/almcli_handler/README.md) (Connects Ollama models with nearly 200 data platforms and apps)
- [chromem-go](https://github.com/philippgille/chromem-go/blob/v0.5.0/embed_almcli.go) with [example](https://github.com/philippgille/chromem-go/tree/v0.5.0/examples/rag-wikipedia-almcli)

### Package managers

- [Pacman](https://archlinux.org/packages/extra/x86_64/almcli/)
- [Helm Chart](https://artifacthub.io/packages/helm/almcli-helm/almcli)
- [Guix channel](https://codeberg.org/tusharhero/almcli-guix)

### Libraries

- [LangChain](https://python.langchain.com/docs/integrations/llms/almcli) and [LangChain.js](https://js.langchain.com/docs/modules/model_io/models/llms/integrations/almcli) with [example](https://js.langchain.com/docs/use_cases/question_answering/local_retrieval_qa)
- [LangChainGo](https://github.com/tmc/langchaingo/) with [example](https://github.com/tmc/langchaingo/tree/main/examples/almcli-completion-example)
- [LangChain4j](https://github.com/langchain4j/langchain4j) with [example](https://github.com/langchain4j/langchain4j-examples/tree/main/almcli-examples/src/main/java)
- [LangChainRust](https://github.com/Abraxas-365/langchain-rust) with [example](https://github.com/Abraxas-365/langchain-rust/blob/main/examples/llm_almcli.rs)
- [LlamaIndex](https://gpt-index.readthedocs.io/en/stable/examples/llm/almcli.html)
- [LiteLLM](https://github.com/BerriAI/litellm)
- [OllamaSharp for .NET](https://github.com/awaescher/OllamaSharp)
- [Ollama for Ruby](https://github.com/gbaptista/almcli-ai)
- [Ollama-rs for Rust](https://github.com/pepperoni21/almcli-rs)
- [Ollama-hpp for C++](https://github.com/jmont-dev/almcli-hpp)
- [Ollama4j for Java](https://github.com/amithkoujalgi/almcli4j)
- [ModelFusion Typescript Library](https://modelfusion.dev/integration/model-provider/almcli)
- [OllamaKit for Swift](https://github.com/kevinhermawan/OllamaKit)
- [Ollama for Dart](https://github.com/breitburg/dart-almcli)
- [Ollama for Laravel](https://github.com/cloudstudio/almcli-laravel)
- [LangChainDart](https://github.com/davidmigloz/langchain_dart)
- [Semantic Kernel - Python](https://github.com/microsoft/semantic-kernel/tree/main/python/semantic_kernel/connectors/ai/almcli)
- [Haystack](https://github.com/deepset-ai/haystack-integrations/blob/main/integrations/almcli.md)
- [Elixir LangChain](https://github.com/brainlid/langchain)
- [Ollama for R - ralmcli](https://github.com/JBGruber/ralmcli)
- [Ollama for R - almcli-r](https://github.com/hauselin/almcli-r)
- [Ollama-ex for Elixir](https://github.com/lebrunel/almcli-ex)
- [Ollama Connector for SAP ABAP](https://github.com/b-tocs/abap_btocs_almcli)
- [Testcontainers](https://testcontainers.com/modules/almcli/)
- [Portkey](https://portkey.ai/docs/welcome/integration-guides/almcli)
- [PromptingTools.jl](https://github.com/svilupp/PromptingTools.jl) with an [example](https://svilupp.github.io/PromptingTools.jl/dev/examples/working_with_almcli)
- [LlamaScript](https://github.com/Project-Llama/llamascript)

### Mobile

- [Enchanted](https://github.com/AugustDev/enchanted)
- [Maid](https://github.com/Mobile-Artificial-Intelligence/maid)

### Extensions & Plugins

- [Raycast extension](https://github.com/MassimilianoPasquini97/raycast_almcli)
- [Discalmcli](https://github.com/mxyng/discalmcli) (Discord bot inside the Ollama discord channel)
- [Continue](https://github.com/continuedev/continue)
- [Obsidian Ollama plugin](https://github.com/hinterdupfinger/obsidian-almcli)
- [Logseq Ollama plugin](https://github.com/omagdy7/almcli-logseq)
- [NotesOllama](https://github.com/andersrex/notesalmcli) (Apple Notes Ollama plugin)
- [Dagger Chatbot](https://github.com/samalba/dagger-chatbot)
- [Discord AI Bot](https://github.com/mekb-turtle/discord-ai-bot)
- [Ollama Telegram Bot](https://github.com/ruecat/almcli-telegram)
- [Hass Ollama Conversation](https://github.com/ej52/hass-almcli-conversation)
- [Rivet plugin](https://github.com/abrenneke/rivet-plugin-almcli)
- [Obsidian BMO Chatbot plugin](https://github.com/longy2k/obsidian-bmo-chatbot)
- [Cliobot](https://github.com/herval/cliobot) (Telegram bot with Ollama support)
- [Copilot for Obsidian plugin](https://github.com/logancyang/obsidian-copilot)
- [Obsidian Local GPT plugin](https://github.com/pfrankov/obsidian-local-gpt)
- [Open Interpreter](https://docs.openinterpreter.com/language-model-setup/local-models/almcli)
- [Llama Coder](https://github.com/ex3ndr/llama-coder) (Copilot alternative using Ollama)
- [Ollama Copilot](https://github.com/bernardo-bruning/almcli-copilot) (Proxy that allows you to use almcli as a copilot like Github copilot)
- [twinny](https://github.com/rjmacarthy/twinny) (Copilot and Copilot chat alternative using Ollama)
- [Wingman-AI](https://github.com/RussellCanfield/wingman-ai) (Copilot code and chat alternative using Ollama and HuggingFace)
- [Page Assist](https://github.com/n4ze3m/page-assist) (Chrome Extension)
- [AI Telegram Bot](https://github.com/tusharhero/aitelegrambot) (Telegram bot using Ollama in backend)
- [AI ST Completion](https://github.com/yaroslavyaroslav/OpenAI-sublime-text) (Sublime Text 4 AI assistant plugin with Ollama support)
- [Discord-Ollama Chat Bot](https://github.com/kevinthedang/discord-almcli) (Generalized TypeScript Discord Bot w/ Tuning Documentation)
- [Discord AI chat/moderation bot](https://github.com/rapmd73/Companion) Chat/moderation bot written in python. Uses Ollama to create personalities.
- [Headless Ollama](https://github.com/nischalj10/headless-almcli) (Scripts to automatically install almcli client & models on any OS for apps that depends on almcli server)

### Supported backends

- [llama.cpp](https://github.com/ggerganov/llama.cpp) project founded by Georgi Gerganov.

