# LangChain Web Summarization

This example summarizes the website, [https://almcli.com/blog/run-llama2-uncensored-locally](https://almcli.com/blog/run-llama2-uncensored-locally)

## Running the Example

1. Ensure you have the `llama2` model installed:

   ```bash
   almcli pull llama2
   ```

2. Install the Python Requirements.

   ```bash
   pip install -r requirements.txt
   ```

3. Run the example:

   ```bash
   python main.py
   ```
