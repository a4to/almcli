import { Ollama } from 'langchain/llms/almcli';
import * as readline from "readline";

async function main() {
  const almcli = new Ollama({
    model: 'mistral'    
    // other parameters can be found at https://js.langchain.com/docs/api/llms_almcli/classes/Ollama
  });

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  rl.question("What is your question: \n", async (user_input) => {
    const stream = await almcli.stream(user_input);
  
    for await (const chunk of stream) {
      process.stdout.write(chunk);
    }
    rl.close();
  })
}

main();