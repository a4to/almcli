#!/bin/sh

set -eu

export VERSION=${VERSION:-0.0.0}
export GOFLAGS="'-ldflags=-w -s \"-X=gitlab.com/a4to/almcli/version.Version=$VERSION\" \"-X=gitlab.com/a4to/almcli/server.mode=release\"'"

docker build \
    --push \
    --platform=linux/arm64,linux/amd64 \
    --build-arg=VERSION \
    --build-arg=GOFLAGS \
    -f Dockerfile \
    -t almcli/almcli -t almcli/almcli:$VERSION \
    .
